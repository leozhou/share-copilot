package main

import (
	"sync"
)

type Config struct {
	Server struct {
		Domain   string `json:"domain"`
		Host     string `json:"host"`
		Port     int    `json:"port"`
		CertPath string `json:"certPath"`
		KeyPath  string `json:"keyPath"`
	} `json:"server"`
	CopilotConfig struct {
		GithubApiUrl string   `json:"github_api_url"`
		Token        []string `json:"token"`
	} `json:"copilot_config"`
	Verification string `json:"verification"`
}

var (
	//初始化需要返回给客户端的响应体
	tokenMap = make(map[string]map[string]interface{})
	//有效的token列表
	validTokenList    = make(map[string]bool)
	requestCountMutex sync.Mutex
	githubApiCount    = 0
	requestCount      = 0
	successCount      = 0
	configFile        Config
)
